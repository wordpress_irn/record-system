# Project setup

```
cd /var/www/
git clone git@gitlab.com:imranfdo/wordpress.git (clone with SSH)
git clone https://gitlab.com/imranfdo/wordpress.git (clone with HTTPS)
cd /var/www/wordpress
composer update
add wp-config.php
```

## Creating wp-config.php
- Add the database details in "Database settings"
- Add the authentication keys. Generate keys from [here](https://api.wordpress.org/secret-key/1.1/salt/)
- Add WP_HOME `define('WP_HOME', 'http://wordpress.local')`
- Add WP_SITEURL `define('WP_SITEURL', 'http://wordpress.local')`
