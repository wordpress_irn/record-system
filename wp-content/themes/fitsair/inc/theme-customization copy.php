<?php

// Create record post type
function create_post_type_record()
{

  $labels = array(
    'name'                  => _x('Records', 'Post Type General Name', 'text_domain'),
    'singular_name'         => _x('Record', 'Post Type Singular Name', 'text_domain'),
    'menu_name'             => __('Records', 'text_domain'),
    'name_admin_bar'        => __('Record', 'text_domain'),
    'archives'              => __('Record Archives', 'text_domain'),
    'attributes'            => __('Record Attributes', 'text_domain'),
    'parent_item_colon'     => __('Parent Record:', 'text_domain'),
    'all_items'             => __('All Records', 'text_domain'),
    'add_new_item'          => __('Add New Record', 'text_domain'),
    'add_new'               => __('Add New Record', 'text_domain'),
    'new_item'              => __('New Record', 'text_domain'),
    'edit_item'             => __('Edit Record', 'text_domain'),
    'update_item'           => __('Update Record', 'text_domain'),
    'view_item'             => __('View Record', 'text_domain'),
    'view_items'            => __('View Records', 'text_domain'),
    'search_items'          => __('Search Record', 'text_domain'),
    'not_found'             => __('Not found', 'text_domain'),
    'not_found_in_trash'    => __('Not found in Trash', 'text_domain'),
    'featured_image'        => __('Featured Image', 'text_domain'),
    'set_featured_image'    => __('Set featured image', 'text_domain'),
    'remove_featured_image' => __('Remove featured image', 'text_domain'),
    'use_featured_image'    => __('Use as featured image', 'text_domain'),
    'insert_into_item'      => __('Insert into record', 'text_domain'),
    'uploaded_to_this_item' => __('Uploaded to this record', 'text_domain'),
    'items_list'            => __('Records list', 'text_domain'),
    'items_list_navigation' => __('Records list navigation', 'text_domain'),
    'filter_items_list'     => __('Filter records list', 'text_domain'),
  );
  $rewrite = array(
    'slug'                  => 'record',
    'with_front'            => true,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __('Record', 'text_domain'),
    'description'           => __('Post Type Description', 'text_domain'),
    'labels'                => $labels,
    'supports'              => array('title', 'custom-fields', 'post-formats'),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-media-spreadsheet',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'query_var'             => 'record',
    'rewrite'               => $rewrite,
  );
  register_post_type('record', $args);
}


// Change record post type title placeholder
function change_cp_record_title()
{
  function record_post_type_title_placeholder($title, $post)
  {
    if ($post->post_type == 'record') {
      $my_title = "AWB No";
      return $my_title;
    }

    return $title;
  }
  add_filter('enter_title_here', 'record_post_type_title_placeholder', 20, 2);
}


// add custom script
function fitsair_theme_custom_scripts()
{
  function fitsair_load_admin_scripts()
  {
    wp_enqueue_media();
    wp_register_script('fitsair-admin-script', get_template_directory_uri() . '/js/custom-admin.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('fitsair-admin-script');
  }
  add_action('admin_enqueue_scripts', 'fitsair_load_admin_scripts');
}


// Disable password field from non administrator roles
function hide_password_non_admin()
{
  function disable_password_fields()
  {
    if (!current_user_can('administrator')) {
      add_filter('show_password_fields', '__return_false');
    }
  }
  if (is_admin()) {
    add_action('init', 'disable_password_fields', 10);
  }
}






// add custom post meta after update the post
add_action('post_updated', 'update_user_group', 10, 3);
function update_user_group($post_id, $post_after, $post_before)
{

  $post_type = get_post_type($post_id);
  // $event_datee = get_post_meta( $post_id, '_EventStartDate', true );
  if ($post_type == 'post') {
    // $month = date( "m",strtotime( $event_datee ) );
    // update_post_meta( $post_id, 'event_month', $month );
    add_post_meta($post_id, 'user_group', 'Alpha', true);
  }
}


$current_user = wp_get_current_user();
// echo "<pre>";
// print_r($current_user);
if ($current_user->has_cap('display_user_group_records')) {
  // var_dump("display_user_group_records");

  function filter_by_user_club($query)
  {
    // echo "<pre>";
    //   print_r($query);
    if (is_admin()) {
      // echo "<pre>";
      // print_r(is_admin());
      $user_id        = get_current_user_id();
      $user_location  = get_user_meta($user_id, 'user_group', 'Alpha');

      // $user_location  = 'Alpha';
      $currentscreen  = get_current_screen();

      $sticky = get_option('user_group');

      // if ($currentscreen->post_type == 'post') {
      // $args = array(
      // 'post_type' => 'post',
      // 'posts_per_page'      => 1,
      // 'post__in'            => $sticky
      // 'ignore_sticky_posts' => 1,
      // );
      // $query = new WP_Query($args);

      // do_action_ref_array('pre_get_posts',  $query);
      // }

      // if ($currentscreen->post_type == 'post') {
      // if (!empty($user_location)) {
      // $location_meta_query = array(
      //   'key'   => 'user_group',
      //   'value' => $user_location,
      //   'compare'   => 'IN'
      // );
      // $query->set('post__in', array($location_meta_query));

      // echo "<pre>";
      // print_r($query);
      // }
      // }
    }
  }
  add_action('pre_get_posts', 'filter_by_user_club', 9998);
}

// Register custom post type
function fitsair_theme_custom_func()
{
  create_post_type_record();
}


// Hooking up our function to theme setup
add_action('init', 'fitsair_theme_custom_func');



// global $wpdb;
// $group_table = $wpdb->prefix.'groups_group';
// $groups_user_group_table = $wpdb->prefix.'groups_user_group';
// $result = $wpdb->get_results( "SELECT * FROM $group_table WHERE `name` = 'Alpha' " );

// $sql = "SELECT kategorie.id, ogloszenia.kategoria, klient_id ".
//     "FROM {$wpdb->prefix}category kategorie ".
//     "JOIN {$wpdb->prefix}ogloszenia_kupione ogloszenia ON (ogloszenia.id = kategorie.id)";

// $results = $wpdb->get_results( $sql );

// var_dump($result);





// $current_user = wp_get_current_user();
// $user_id = $current_user->ID;

// apply_filters('show_password_fields', '__return_false', $current_user);
// echo "<pre>";
// print_r($current_user);
// var_dump($current_user->ID);
// if ($current_user->has_cap('edit_users')) {
//   // do something
// }


// add user group to user mete
// function add_user_group()
// {
//   add_action('user_register', function ($user_id) {
//     global $wpdb;
//     $group_ids = $_POST['group_ids'][0];

//     $group_table = $wpdb->prefix . 'groups_group';
//     $result = $wpdb->get_results("SELECT name FROM $group_table WHERE group_id = $group_ids ", ARRAY_A);
//     $user_group_name = $result[0]['name'];
//     $user_group = $user_group_name;

//     update_user_meta($user_id, 'user_group', $user_group, true);
//   });
// }