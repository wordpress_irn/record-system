<?php

// Create record post type
function create_post_type_record()
{

  $labels = array(
    'name'                  => _x('Records', 'Post Type General Name', 'text_domain'),
    'singular_name'         => _x('Record', 'Post Type Singular Name', 'text_domain'),
    'menu_name'             => __('Records', 'text_domain'),
    'name_admin_bar'        => __('Record', 'text_domain'),
    'archives'              => __('Record Archives', 'text_domain'),
    'attributes'            => __('Record Attributes', 'text_domain'),
    'parent_item_colon'     => __('Parent Record:', 'text_domain'),
    'all_items'             => __('All Records', 'text_domain'),
    'add_new_item'          => __('Add New Record', 'text_domain'),
    'add_new'               => __('Add New Record', 'text_domain'),
    'new_item'              => __('New Record', 'text_domain'),
    'edit_item'             => __('Edit Record', 'text_domain'),
    'update_item'           => __('Update Record', 'text_domain'),
    'view_item'             => __('View Record', 'text_domain'),
    'view_items'            => __('View Records', 'text_domain'),
    'search_items'          => __('Search Record', 'text_domain'),
    'not_found'             => __('Not found', 'text_domain'),
    'not_found_in_trash'    => __('Not found in Trash', 'text_domain'),
    'featured_image'        => __('Featured Image', 'text_domain'),
    'set_featured_image'    => __('Set featured image', 'text_domain'),
    'remove_featured_image' => __('Remove featured image', 'text_domain'),
    'use_featured_image'    => __('Use as featured image', 'text_domain'),
    'insert_into_item'      => __('Insert into record', 'text_domain'),
    'uploaded_to_this_item' => __('Uploaded to this record', 'text_domain'),
    'items_list'            => __('Records list', 'text_domain'),
    'items_list_navigation' => __('Records list navigation', 'text_domain'),
    'filter_items_list'     => __('Filter records list', 'text_domain'),
  );
  $rewrite = array(
    'slug'                  => 'record',
    'with_front'            => true,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __('Record', 'text_domain'),
    'description'           => __('Post Type Description', 'text_domain'),
    'labels'                => $labels,
    'supports'              => array('title', 'custom-fields', 'post-formats'),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-media-spreadsheet',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'query_var'             => 'record',
    'rewrite'               => $rewrite,
  );
  register_post_type('record', $args);
}


// Change record post type title placeholder
function change_cp_record_title()
{
  function record_post_type_title_placeholder($title, $post)
  {
    if ($post->post_type == 'record') {
      $my_title = "AWB No";
      return $my_title;
    }

    return $title;
  }
  add_filter('enter_title_here', 'record_post_type_title_placeholder', 20, 2);
}


// add custom script
function fitsair_theme_custom_scripts()
{
  function fitsair_load_admin_scripts()
  {
    wp_enqueue_media();
    wp_register_script('fitsair-admin-script', get_template_directory_uri() . '/js/custom-admin.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('fitsair-admin-script');
  }
  add_action('admin_enqueue_scripts', 'fitsair_load_admin_scripts');

  // Update CSS within in Admin
  function fitsair_load_admin_style()
  {
    wp_enqueue_style('fitsair-admin-styles', get_template_directory_uri() . '/css/custom-admin.css');
  }
  add_action('admin_enqueue_scripts', 'fitsair_load_admin_style');
}


// Disable password field from non administrator roles
function hide_password_non_admin()
{
  function disable_password_fields()
  {
    if (!current_user_can('administrator')) {
      add_filter('show_password_fields', '__return_false');
    }
  }
  if (is_admin()) {
    add_action('init', 'disable_password_fields', 10);
  }
}


// hide other roles from user form
function hide_other_user_roles()
{
  remove_role('subscriber');
  remove_role('contributor');
  remove_role('author');
  remove_role('editor');

  // remove additional capabilities dropdown from user page
  add_filter('ure_show_additional_capabilities_section', 'ure_show_additional_capabilities_section');
  add_filter('ure_bulk_grant_roles', 'ure_show_additional_capabilities_section');

  function ure_show_additional_capabilities_section($show)
  {
    if (current_user_can('administrator')) {
      $show = false;
    }

    return $show;
  }
}


// add user_group meta when record add
function insert_record_after()
{
  // add custom post meta after update the post
  add_action('post_updated', 'update_user_group', 10, 3);
  function update_user_group($post_id, $post_after, $post_before)
  {
    $post_type = get_post_type($post_id);
    $current_user_id = get_current_user_id();
    $user_group_key = 'user_group';
    $user_group_name = get_user_meta($current_user_id,  $user_group_key);
    if ($post_type == 'post' || $post_type == 'record') {
      add_post_meta($post_id, $user_group_key, $user_group_name, true);
    }
  }
}


// record list
function modify_record_list()
{
  add_filter('manage_record_posts_columns', 'bs_record_table_head');
  function bs_record_table_head($defaults)
  {
    $defaults['cross_weight']  = 'Cross Weight';
    // $defaults['route']    = 'Ticket Status';
    $defaults['agent']   = 'Venue';
    // $defaults['author'] = 'Added By';
    return $defaults;
  }

  add_action('manage_record_posts_custom_column', 'bs_record_table_content', 10, 2);
  function bs_record_table_content($column_name, $post_id)
  {
    if ($column_name == 'cross_weight') {
      echo get_post_meta($post_id, 'cross_weight', true);
      // $record_date = get_post_meta($post_id, '_cross_weight', true);
      // echo  date(_x('F d, Y', 'record date format', 'textdomain'), strtotime($record_date));
    }
    // if ($column_name == 'route') {
    //   $status = get_post_meta($post_id, '_route', true);
    //   echo $status;
    // }

    if ($column_name == 'agent') {
      echo get_post_meta($post_id, 'agent', true);
    }
  }
}

// Register custom post type
function fitsair_theme_custom_func()
{
  create_post_type_record();
  change_cp_record_title();
  fitsair_theme_custom_scripts();
  hide_password_non_admin();
  hide_other_user_roles();
  insert_record_after();
  modify_record_list();
}

// Hooking up our function to theme setup
add_action('init', 'fitsair_theme_custom_func');
